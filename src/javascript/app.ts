import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { IFighter } from './IFighter';

const rootElement : HTMLElement = document.getElementById('root');
const loadingElement : HTMLElement = document.getElementById('loading-overlay');

export async function startApp() : Promise<void> {
  try {
    loadingElement.style.visibility = 'visible';
    
    const fighters : IFighter[] = await getFighters();
    const fightersElement : HTMLElement = createFighters(fighters);

    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}
