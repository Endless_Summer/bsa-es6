import { IFighterInfo } from "./IFighterInfo";

export function fight( firstFighter: IFighterInfo, secondFighter: IFighterInfo) : IFighterInfo{
  // console.log(firstFighter);
  // console.log(secondFighter);

  let winner : IFighterInfo;

  while(true) {

    let damageFirstFighter : number = getDamage(firstFighter, secondFighter);
    secondFighter.health -= damageFirstFighter;

    if(secondFighter.health <= 0){
      winner = firstFighter;
      break;
    }

    let damageSecondFighter : number = getDamage(secondFighter, firstFighter);
    firstFighter.health -= damageSecondFighter;

    if(firstFighter.health <= 0){
      winner = secondFighter;
      break;
    }

    // console.log('1: ' + firstFighter.health + ' 2: ' + secondFighter.health);
  }
  // console.log(winner);
  return winner;
}

export function getDamage(attacker: IFighterInfo, enemy: IFighterInfo) : number{
  const damage : number = getHitPower(attacker) - getBlockPower(enemy);
  if(damage > 0)
    return damage;
  else
    return 0;
}

export function getHitPower(fighter : IFighterInfo) : number{

  const criticalHitChance : number = Math.random() + 1;
  return fighter.attack * criticalHitChance;

}

export function getBlockPower(fighter: IFighterInfo) : number{
  let dodgeChance : number = Math.random() + 1;
  return fighter.defense + dodgeChance;
}
