import { callApi } from '../helpers/apiHelper';
import { IFighterInfo } from '../IFighterInfo';
import { IFighter } from '../IFighter';


export async function getFighters() : Promise<IFighter[]> {
  try {
    const endpoint : string = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');

    // console.log(apiResult);
    return apiResult as IFighter[];
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id : number) : Promise<IFighterInfo>{
  try {

    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    // console.log(apiResult);
    return apiResult as IFighterInfo;
  } catch (error) {
    throw error;
  }

}

