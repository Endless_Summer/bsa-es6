import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';
import { IFighterInfo } from './IFighterInfo';
import { IFighter } from './IFighter';

export function createFighters(fighters : IFighter[]) : HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements : HTMLElement[] = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer : HTMLElement = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

// const fightersDetailsCache = new Map();

async function showFighterDetails(event : Event, fighter : IFighter) : Promise<void> {
  const fullInfo  : IFighterInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) : Promise<IFighterInfo>{
  // get fighter form fightersDetailsCache or use getFighterDetails function

  return getFighterDetails(parseInt(fighterId));
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterInfo>();

  return async function selectFighterForBattle(event : Event, fighter : IFighter)  {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {

      let mapIter : IterableIterator<string>= selectedFighters.keys();
      let key1 : string = mapIter.next().value;
      let key2 : string = mapIter.next().value;

      let firstFighter : IFighterInfo = selectedFighters.get(key1);
      let secondFighter : IFighterInfo = selectedFighters.get(key2);

      const winner : IFighterInfo = fight(firstFighter, secondFighter);

      // console.log(winner);
      showWinnerModal(winner);
    }
  }
}
