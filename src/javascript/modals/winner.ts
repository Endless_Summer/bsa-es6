import { IFighterInfo } from "../IFighterInfo";
import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';

export  function showWinnerModal(fighter : IFighterInfo) : void {
  // show winner name and image
  const title : string = 'Winner';

  const bodyElement : HTMLElement = createWinner(fighter);

  showModal({ title, bodyElement });
}

function createWinner(fighter : IFighterInfo) : HTMLElement{

  const winnerDetails : HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement : HTMLElement = createElement({ tagName: 'span', className: 'fighter-name' });
  nameElement.innerText = `Name : ${fighter.name}`;

  const imageElement : HTMLImageElement = createElement({ tagName: 'img', className: 'fighter-image'}) as HTMLImageElement;
  imageElement.src = fighter.source;

  winnerDetails.append(nameElement, imageElement);

  return winnerDetails;
}
