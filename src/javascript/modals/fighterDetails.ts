import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { IFighterInfo } from '../IFighterInfo';

export  function showFighterDetailsModal(fighter : IFighterInfo) : void {
  const title : string = 'Fighter info';
  const bodyElement : HTMLElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter : IFighterInfo) : HTMLElement{
  const { name } = fighter;
  const { attack } = fighter;
  const { defense } = fighter;
  const { health } = fighter;

  const fighterDetails : HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement : HTMLElement = createElement({ tagName: 'span', className: 'fighter-name' });
  
  // show fighter name, attack, defense, health, image

  let html: string = 'Name: ' + name;
  html += '\nAttack: ' + attack;
  html += '\nDefense: ' + defense;
  html += '\nHealth: ' + health;

  nameElement.innerText = html;

  fighterDetails.append(nameElement);

  return fighterDetails;
}
